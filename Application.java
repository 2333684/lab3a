

public class Application 
{
	public static void main (String [] args) {
		
		Student studentOne = new Student();
		
		studentOne.name = "Ryan";
		studentOne.program = "Computer Science Technologie";
		studentOne.average = 94.86;
		
		System.out.println(studentOne.name);
		System.out.println(studentOne.program);
		System.out.println(studentOne.average);
		
		studentOne.greetingToCS();
		studentOne.rateGrade();
		
		Student studentTwo = new Student();
		
		studentTwo.name = "Jorge";
		studentTwo.program = "General Social Science";
		studentTwo.average = 60;
		
		System.out.println(studentTwo.name);
		System.out.println(studentTwo.program);
		System.out.println(studentTwo.average);
		
		studentTwo.greetingToCS();
		studentTwo.rateGrade();
		
		Student studentThree = new Student();
		
		studentTwo.name = "Mich";
		studentTwo.program = "Pure and Applied";
		studentTwo.average = 76;
		
		System.out.println(studentThree.name);
		System.out.println(studentThree.program);
		System.out.println(studentThree.average);
		
		studentThree.greetingToCS();
		studentThree.rateGrade();
		
		Student[] section3 = new Student[3];
		section3[0]= studentOne;
		section3[1]= studentTwo;
		section3[2]= studentThree;
		
		System.out.println(section3[2].name);
	}
}
		